const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    getAll() {
        return UserRepository.getAll();
    }
    getOne(id) {
        return UserRepository.getOne(id);
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
    create(user) {

        if (!this.getAll().find(item => (item.email.toLowerCase() === user.email.toLowerCase()) || (item.phoneNumber.toLowerCase() === user.phoneNumber.toLowerCase()))) {
            UserRepository.create(user);
        } else {
            throw Error('User with this email or phone number already exists!');
        }
    }
    update(param, user) {
        return UserRepository.update(param, user);
    }
    delete(param) {
        return UserRepository.delete(param);
    }
}

module.exports = new UserService();