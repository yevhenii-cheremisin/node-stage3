const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getAll() {
        return FighterRepository.getAll();
    }
    getOne(id) {
        return FighterRepository.getOne(id);
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
    create(fighter) {
        if (!this.getAll().find(item => (item.name.toLowerCase() === fighter.name.toLowerCase()))) {
            FighterRepository.create(fighter);
        } else {
            throw new Error('Fighter with this name already exists');
        }
    }
    update(param, fighter) {
        if (!this.getAll().find(item => (item.name.toLowerCase() === fighter.name.toLowerCase()))) {
            return FighterRepository.update(param, fighter);
        } else {
            throw new Error('Fighter with this name already exists');
        }
    }
    delete(param) {
        return FighterRepository.delete(param);
    }
}

module.exports = new FighterService();