const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const { request } = require('chai');

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/', (req, res, next) => {
    res.body = FighterService.getAll();
    next();
}, responseMiddleware)

router.get('/:id', (req, res, next) => {
    const id = req.params.id;
    res.body = FighterService.getOne({ id });
    if (res.body) {
        res.isExist = false;
    } else {
        res.isExist = true;
    }
    next();
}, responseMiddleware)

router.post('/', createFighterValid, (req, res, next) => {
    try {
        if (!(res.error instanceof Error)) {
            FighterService.create(req.body);
            res.body = req.body;
        }
    } catch (err) {
        res.error = err;
    } finally {
        next();
    }
}, responseMiddleware)

router.put('/:id', updateFighterValid, (req, res, next) => {
    try {
        if (!(res.error instanceof Error)) {
            const id = req.params.id;
            const isUser = FighterService.getOne({ id });
            const updatedData = FighterService.update(req.params.id, req.body);
            res.body = updatedData;
            if (isUser) {
                res.isExist = false;
            } else {
                res.isExist = true;
            }
        }
    } catch (err) {
        res.error = err;
    } finally {
        next();
    }
}, responseMiddleware)

router.delete('/:id', (req, res, next) => {
    const id = req.params.id;
    const isUser = FighterService.getOne({ id });
    const deletedData = FighterService.delete(id, req.body);
    res.body = deletedData;
    if (isUser) {
        res.isExist = false;
    } else {
        res.isExist = true;
    }
    next();
}, responseMiddleware)

module.exports = router;