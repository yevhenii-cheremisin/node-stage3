const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.get('/', (req, res, next) => {
    res.body = UserService.getAll();
    next();
}, responseMiddleware)

router.get('/:id', (req, res, next) => {
    const id = req.params.id;
    res.body = UserService.getOne({ id });
    if (res.body) {
        res.isExist = false;
    } else {
        res.isExist = true;
    }
    next();
}, responseMiddleware)

router.post('/', createUserValid, (req, res, next) => {
    try {
        if (!(res.error instanceof Error)) {
            UserService.create(req.body);
            res.body = req.body;
        }
    } catch (err) {
        res.error = err;
    } finally {
        next();
    }

}, responseMiddleware)

router.put('/:id', updateUserValid, (req, res, next) => {
    try {
        if (!(res.error instanceof Error)) {
            const id = req.params.id;
            const isUser = UserService.getOne({ id });
            const updatedData = UserService.update(id, req.body);
            res.body = updatedData;
            if (isUser) {
                res.isExist = false;
            } else {
                res.isExist = true;
            }
        }
    } catch (err) {
        res.error = err;
    } finally {
        next();
    }
}, responseMiddleware)

router.delete('/:id', (req, res, next) => {
    const id = req.params.id;
    const isUser = UserService.getOne({ id });
    const deletedData = UserService.delete(id, req.body);
    res.body = deletedData;
    if (isUser) {
        res.isExist = false;
    } else {
        res.isExist = true;
    }
    next();
}, responseMiddleware)

module.exports = router;