const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const data = req.body;

    if (data.id) {
        res.error = new Error('Id is illegal to write');
        next();
    }

    if (!(data.firstName) || !(data.lastName)) {
        res.error = new Error('Please enter your name and surname');
        next();
    }

    if (!data.email || !((data.email).endsWith('@gmail.com'))) {
        res.error = new Error('Invalid gmail');
        next();
    }

    if (!data.phoneNumber || (data.phoneNumber).length != 13 || !(data.phoneNumber).startsWith('+380')) {
        res.error = new Error('Invalid phone number, length not equial 13');
        next();
    }

    if (!data.password || (data.password).length < 3) {
        res.error = new Error('Invalid password');
        next();
    }

    if (!Object.keys(data).every((key) => {
        return user.hasOwnProperty(key);
    })) {
        res.error = new Error('Unknown property');
        next();
    }

    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const data = req.body;

    if (data.id) {
        res.error = new Error('Id is illegal to write');
        next();
    }

    if (data.email) {
        if (!((data.email).endsWith('@gmail.com'))) {
            res.error = new Error('Invalid gmail');
            next();
        }
    }

    if (data.phoneNumber) {
        if ((data.phoneNumber).length != 13 || !(data.phoneNumber).startsWith('+380')) {
            res.error = new Error('Invalid phone number, length not equial 13');
            next();
        }
    }

    if (data.password) {
        if ((data.password).length <= 3) {
            res.error = new Error('Invalid password');
            next();
        }
    }

    if (!Object.keys(data).every((key) => {
        return user.hasOwnProperty(key);
    })) {
        res.error = new Error('Unknown property');
        next();
    }

    if (!data.firstName && !data.lastName && !data.email && !data.phoneNumber && !data.password) {
        res.error = new Error('Can\'t send an empty object');
        next();
    }

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;