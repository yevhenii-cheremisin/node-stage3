const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    const data = req.body;

    if (!data.name) {
        res.error = new Error('Wrong name to your fighter');
        next();
    }

    if (!data.power || !(data.power > 1) || !(data.power < 100)) {
        res.error = new Error('Wrong power value to your fighter');
        next();
    }

    if (!data.defense || !(data.defense > 1) || !(data.defense < 10)) {
        res.error = new Error('Wrong defense value to your fighter');
        next();
    }

    if (!data.health) {
        data.health = 100;
    }

    if (!(data.health > 80) || !(data.health < 120)) {
        res.error = new Error('Wrong health value to your fighter');
        next();
    }

    if (!Object.keys(data).every((key) => {
        return fighter.hasOwnProperty(key);
    })) {
        res.error = new Error('Unknown property');
        next();
    }

    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const data = req.body;

    if (data.id) {
        res.error = new Error('Id is illegal to write');
        next();
    }

    if (data.name) {
        if (data.name === "") {
            res.error = new Error('Wrong name to your fighter');
            next();
        }
    }

    if (data.power) {
        if (!(data.power > 1) || !(data.power < 100)) {
            res.error = new Error('Wrong power value to your fighter');
            next();
        }
    }

    if (data.defense) {
        if (!(data.defense > 1) || !(data.defense < 10)) {
            res.error = new Error('Wrong defense value to your fighter');
            next();
        }
    }

    if (data.health) {
        if (!(data.health > 80) || !(data.health < 120)) {
            res.error = new Error('Wrong health value to your fighter');
            next();
        }
    }

    if (!Object.keys(data).every((key) => {
        return fighter.hasOwnProperty(key);
    })) {
        res.error = new Error('Unknown property');
        next();
    }

    if (!data.name && !data.power && !data.defense && !data.health) {
        res.error = new Error('Can\'t send an empty object');
        next();
    }

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;