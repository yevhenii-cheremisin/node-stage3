const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query


    if (res.isExist) {
        res.status(404).send({
            error: true,
            message: "404 Page not Found"
        });
        return false;
    }

    if (res.error instanceof Error) {
        res.status(400).send({
            error: true,
            message: res.error.message
        });
        return false;
    } else {
        res.status(200).send(res.body);
        return;
    }

}

exports.responseMiddleware = responseMiddleware;